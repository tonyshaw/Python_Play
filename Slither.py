import pygame, sys, time, random
from pygame.locals import *

pygame.init()
fpsClock = pygame.time.Clock()

playSurface = pygame.display.set_mode((640, 480))
pygame.display.set_caption('Raspberry Snake')
# pygame.display.set_position(100, 100)

def randColour():
    colour = pygame.Color(random.randrange(0, 255), random.randrange(0, 255), random.randrange(0, 255))
    return colour

redColour = pygame.Color(0, 255, 0)
blackColour = pygame.Color(155, 0, 100)
whiteColour = pygame.Color(0, 100, 200)
greyColour = pygame.Color(150, 150, 150)
raspberryColour = randColour()

snakePosition = [100, 100]
snakeSegments = [[100, 100], [80, 100], [60, 100]]
snakeSegmentsColours = [randColour(), randColour(), randColour()]

raspberryPosition = [300, 300]
raspberrySpawned = 1
direction = 'right'
changeDirection = direction


def gameOver():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 72)
    gameOverSurf = gameOverFont.render('game over', True, greyColour)
    gameOverRect = gameOverSurf.get_rect()
    gameOverRect.midtop = (320, 10)
    playSurface.blit(gameOverSurf, gameOverRect)
    pygame.display.flip()
    time.sleep(5)
    pygame.quit()
    sys.exit()

time.sleep(2)
while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN:                       
            if event.key == K_RIGHT or event.key == ord('d'):
                changeDirection = 'right'
            if event.key == K_LEFT or event.key == ord('a'):
                changeDirection = 'left'
            if event.key == K_UP or event.key == ord('w'):
                changeDirection = 'up'
            if event.key == K_DOWN or event.key == ord ('s'):
                changeDirection = 'down'
            if event.key == K_ESCAPE:
                pygame.event.post(pygame.event.Event(QUIT))

    if changeDirection == 'right' and not direction == 'left':
        direction = changeDirection
    if changeDirection == 'left' and not direction == 'right':
        direction = changeDirection
    if changeDirection == 'up' and not direction == 'down':
        direction = changeDirection
    if changeDirection == 'down' and not direction == 'up':
        direction = changeDirection

    if direction == 'right':
        snakePosition[0] += 20                             
    if direction == 'left':                                                                             
        snakePosition[0] -= 20
    if direction == 'up':
        snakePosition[1] -= 20
    if direction == 'down':
        snakePosition[1] += 20

    snakeSegments.insert(0, list(snakePosition))

    if snakePosition[0] == raspberryPosition[0] and snakePosition[1] == raspberryPosition[1]:
        raspberrySpawned = 0
        snakeSegmentsColours.insert(0, raspberryColour)
        raspberryColour = randColour()
    else:
        snakeSegments.pop()
    
    if raspberrySpawned == 0:
        x = random.randrange(3, 30)
        y = random.randrange(3, 22)
        raspberryPosition = [int(x*20), int(y*20)]
    raspberrySpawned = 1
    playSurface.fill(blackColour)
    i = 0
    for position in snakeSegments:
        segmentColour = snakeSegmentsColours[i]
        pygame.draw.rect(playSurface, segmentColour, Rect(position[0], position[1], 20, 20))
        i += 1
    pygame.draw.rect(playSurface, raspberryColour, Rect(raspberryPosition[0], raspberryPosition[1], 20, 20))
    pygame.display.flip()
    if snakePosition[0] > 620:
        snakePosition[0] = 0
    if snakePosition[0] < 0:
        snakePosition[0] = 620
    if snakePosition[1] > 460:
        snakePosition[1] = 0
    if snakePosition[1] < 0:
        snakePosition[1] = 460
    for snakeBody in snakeSegments[1:]:
        if snakePosition[0] == snakeBody[0] and snakePosition[1] == snakeBody[1]:
            gameOver()
    fpsClock.tick(10)
