import pygame
import pygame, sys, time, random

screen = pygame.display.set_mode((640, 480))
running = 1

def randColour():
    colour = pygame.Color(random.randrange(0, 255), random.randrange(0, 255), random.randrange(0, 255))
    return colour
 
while running:
   event = pygame.event.poll()
   if event.type == pygame.QUIT:
       running = 0

   screen.fill((0, 0, 0))
   for i in range(1, 20):
       pygame.draw.line(screen, randColour(),
                        (random.randrange(0, 639), random.randrange(0, 479)),
                        (random.randrange(0, 639), random.randrange(0, 479)))
   pygame.display.flip()
   time.sleep(1)
   print("iter")
   print(event)

pygame.quit()
sys.exit()
